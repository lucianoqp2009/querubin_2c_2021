/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/3_Regla_Digital.h"       /* <= own header */
#include "hc_sr4.h"
#include "systemclock.h"
#include "gpio.h"
#include "delay.h"
#include "uart.h"
#include "led.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	int8_t a=0, b=0, teclas, lectura=FALSE, hold=FALSE;
	SystemClockInit();
	serial_config ua={SERIAL_PORT_PC, 9600, NULL};
	UartInit(&ua);
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	LedsInit();
	SwitchesInit();

	   while (1){
		   if (lectura==TRUE){
			   if (hold==FALSE){
		   b = HcSr04ReadDistanceCentimeters();

		   UartSendString(SERIAL_PORT_PC, UartItoa(b, 10));
		   UartSendString(SERIAL_PORT_PC, "\r\n");

	if (b<10){
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if (b>10&&b<20){
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if(b>20&&b<30){
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}
	if(b>30) {
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
	}
	DelayMs(500);
		   }else {
			   a=b;
			   UartSendString(SERIAL_PORT_PC, UartItoa(a, 10));

		   }} else {
			   b=0;
			   UartSendString(SERIAL_PORT_PC, UartItoa(b, 10));
			   UartSendString(SERIAL_PORT_PC, "\r\n");
			   DelayMs(500);
		   }
	teclas = SwitchesRead();
	switch(teclas){
		case SWITCH_1:
			lectura=!lectura;
			DelayMs(500);
			break;
		case SWITCH_2:
			hold=!hold;
			DelayMs(500);
			break;
	}}

	return 0;
}

/*==================[end of file]============================================*/

