/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/HcSr4_Ultrasonido.h"       /* <= own header */
#include "hc_sr4.h"
#include "systemclock.h"
#include "gpio.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	int8_t a=1,b=0;
    SystemClockInit();
    HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

   while (a=1){
	   b = HcSr04ReadDistanceCentimeters();
	   //printf(b);
	   DelayMs(1);
   }

    
	return 0;
}

/*==================[end of file]============================================*/

