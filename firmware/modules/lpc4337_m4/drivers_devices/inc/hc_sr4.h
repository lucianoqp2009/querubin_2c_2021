/*
 * hc_sr4.h
 *
 *  Created on: 3 de set. de 2021
 *      Author: alumno
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_
/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup LED Led
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 ** This is a driver for six leds mounted on the board
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 *  EF			Eduardo Filomena
 *  JMR			Juan Manuel Reta
 *  SM			Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version leo
 * 20190820 v1.1 new version made by Sebastian Mateos
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn void HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Inicializar los puertos triggers y echo del sensor ultrasonido
 * @param[in] Puertos echo y trigger
 * @return FALSE si no se inicializan correctamente
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);


/** @fn int16_t HcSr04ReadDistanceInches(void);
 * @brief Lectura de la distancia del objeto en pulgadas
 * @param[in] No hay parametros
 * @return Un entero de 16 bits que representa el valor en pulgadas de la distancia
 */
int16_t HcSr04ReadDistanceInches(void);

/** @fn int16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Lectura de la distancia del objeto en centimetros
 * @param[in] No hay parametros
 * @return Un entero de 16 bits que representa el valor en pulgadas de la distancia
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief Finaliza los puertos echo y trigger
 * @param[in] gpio_t echo, gpio_t trigger
 * @return FALSE si hubo un error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_ */
