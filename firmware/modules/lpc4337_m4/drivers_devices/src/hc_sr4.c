/*
 * hc_sr4.c
 *
 *  Created on: 3 de set. de 2021
 *      Author: alumno
 */
/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
gpio_t echo_global;
gpio_t trigger_global;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
int16_t HcSr04TiempoEsp(void);//Realiza la lectura del eccho y devuelve el tiempo en Usec
void HcSr04TrigInit(void);//Inicializa la lectura en el periferico
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
void HcSr04TrigInit(void){
	GPIOOn(trigger_global);
	DelayUs(10);
	GPIOOff(trigger_global);
}
int16_t HcSr04TiempoEsp(void){
	int16_t cont=0;
	while (GPIORead(echo_global)==0){
		//DelayUs(1);
	}
	while (GPIORead(echo_global)==1){
		cont++;
		DelayUs(1);
	}
	return (cont);
}
/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger){
	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);
	echo_global = echo;
	trigger_global = trigger;
	return 1;
}
bool HcSr04DeInit(gpio_t echo, gpio_t trigger){
	GPIOOff(echo);
	GPIOOff(trigger);
	return 1;
}
int16_t HcSr04ReadDistanceInches(void){
	int16_t distance=0, TimeUs=0;
	HcSr04TrigInit();
	TimeUs=HcSr04TiempoEsp();
	distance=TimeUs/148;
	return (distance);

}
int16_t HcSr04ReadDistanceCentimeters(void){
	int16_t distance=0, TimeUs=0;
	HcSr04TrigInit();
	TimeUs=HcSr04TiempoEsp();
	distance=TimeUs/30;
	return (distance);

}
