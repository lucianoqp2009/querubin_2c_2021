/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Lucaino querubin
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
typedef struct{
	uint8_t port;
	uint8_t pin;
	uint8_t dir;

}gpioConf_t;
#define IN 0
#define OUT 1
void BCDto7segm (uint8_t digit, gpioConf_t *vector){
	uint8_t i, constante;
	uint8_t mask=1;

	for (i=0; i<sizeof(vector); i++){
		printf("El puerto %d", vector[i].port);
		printf (" pin %d", vector[i].pin);
		constante = digit&mask;
		printf(" tiene un %d \n\n", constante);
		digit = digit>>1;
	}
}
/*==================[internal functions declaration]=========================*/

int main(void)
{
   uint8_t digito=1;
   gpioConf_t puertos[4]={{1,4,OUT},{1,5,OUT},{1,6,OUT},{2,14,OUT}};
   BCDto7segm(digito,puertos);
	return 0;
}

/*==================[end of file]============================================*/

