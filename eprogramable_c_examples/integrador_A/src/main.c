/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Querubin Luciano
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define on 1
#define off 2
#define toggle 3

/*==================[internal functions declaration]=========================*/

	struct leds{
		uint8_t n_led;      //indica el número de led a controlar
		uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
		uint8_t periodo;    //indica el tiempo de cada ciclo
		uint8_t mode;       //ON, OFF, TOGGLE
	} my_leds;
	void LedControl (struct leds*val){
	uint8_t i,j,k;
	switch (val->mode){
		case 1:
			switch (val->n_led){
				case 1: printf("Poner el led %d en alto \r\n",val->n_led);
						break;
				case 2: printf("Poner el led %d en alto \r\n",val->n_led);
						break;
				case 3: printf("Poner el led %d en alto \r\n",val->n_led);
				break;
			}break;
		case 2:
			switch (val->n_led){
				case 1: printf("Poner el led %d en bajo \r\n",val->n_led);
				break;
				case 2: printf("Poner el led %d en bajo \r\n",val->n_led);
				break;
				case 3: printf("Poner el led %d en bajo \r\n",val->n_led);
				break;
			}break;
		case 3:
			switch (val->n_led){
				case 1: for(i=0; i<val->n_ciclos; i++){
					printf("Poner el led %d en alto \r\n",val->n_led);
					for (j=val->periodo; j>0;j--)
						printf("Esperando %d segundos \r\n",j);
					printf("Poner el led %d en bajo \r\n", val->n_led);
					for (k=val->periodo; k>0;k--)
						printf("Esperando %d segundos \r\n",k);
					} break;
				case 2: for(i=0; i<val->n_ciclos; i++){
									printf("Poner el led %d en alto \r\n",val->n_led);
									for (j=val->periodo; j>0;j--)
										printf("Esperando %d segundos \r\n",j);
									printf("Poner el led %d en bajo \r\n", val->n_led);
									for (k=val->periodo; k>0;k--)
										printf("Esperando %d segundos \r\n",k);
									} break;
				case 3: for(i=0; i<val->n_ciclos; i++){
									printf("Poner el led %d en alto \r\n",val->n_led);
									for (j=val->periodo; j>0;j--)
										printf("Esperando %d segundos \r\n",j);
									printf("Poner el led %d en bajo \r\n", val->n_led);
									for (k=val->periodo; k>0;k--)
										printf("Esperando %d segundos \r\n",k);
									} break;

	}break;
	}
	return;}
int main(void)
{
	my_leds.mode=3;
	my_leds.n_ciclos=2;
	my_leds.n_led=2;
	my_leds.periodo=4;
	LedControl(&my_leds);



}

/*==================[end of file]============================================*/

